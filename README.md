# PyThia Demo

## Description

This repository shows some demo applications of the _PyThia Uncertainty Quantification_ Toolbox.
The demos are located in the `nbs/` subdirectory.
To start a demo, run `jupyter-lab`.

## Installation

To install everything you need to run the demos, you can install the conda environment specified in the `environment.yml` file by
```sh
mamba env create --file environment.yml
```

## Usage

The demo scripts give an overview of the syntax _PyThia_ uses and how _PyThia_ can be applied to solve uncertainty quantification problems.

## Support

More information on _PyThia_ as well as a detailed documentation of the modules can be found on the [PyThia Homepage](https://pythia-uq.readthedocs.io/en/latest/).

## Authors and acknowledgment

This repository is developed and maintained by PTB's [Work Group 8.43](https://www.ptb.de/cms/en/ptb/fachabteilungen/abt8/fb-84/ag-8430.html).
